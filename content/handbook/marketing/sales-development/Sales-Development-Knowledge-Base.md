---

title: Sales Development Knowledge Base
description: The purpose of this page is to serve as an in-depth knowledge base for the Sales Development team.

---

## Sales Development Knowledge Base

This page is part of the Sales Development team's handbook [page](https://handbook.gitlab.com/handbook/marketing/sales-development/) and aims to be the in-depth knowledge base of our org. The content below is sourced from the input of many past team members and is here for you to perfect your inbound and outbound sales development skills. 

Please review at your own leasure, and we're excited to receive your contributions as well!

## Best Practices for Outbound BDRs

### Cold-Calling and Email Checklist

Our cold-calling best practices typically consist of 4 elements. A pattern interrupt, an elevator pitch and, if required, objection/trap-setting questions and Up-Front Contracts.

To be effective, these need to be customized to the individuals we call as per the logic below:

- Different Geographical Business Cultures
- Personality Type as per the DISC model
- Individual’s Role and Responsibilities
- Business’ Needs and Objectives

The main documentation may be [found here](https://docs.google.com/document/d/1D3iV_WW5fRidRN5H8-3SZVAAr3ffEvjxUC6cW5SFXDM/edit) and the library of resources [here](https://drive.google.com/drive/folders/1VhxVwy4DoEvFco_wXeOLb0DLtVIfo7wQ).

### Up-Front Contract

Typically used for scheduled calls and Up-Front Contracts. UFCs can also be used as a defense mechanism when cold calling does not go as planned. For cold-calling, UFCs are used when the prospect can commit to 15’ at the time of the call. If they commit to a 15’ for a later time, UFCs are used to open the call and set the agenda then.

Explore the main documentation [here](https://docs.google.com/document/d/1Y7qEq8g3eHh5-oagERGvNmatiOV3JXi9Tw46SKWwpNM/edit).

### Decision Maker Discovery

The questions below are suggestions to be used while doing a discovery call, in order to identify the decision making process, and to meet the outbound accepted SAO as per the guidelines [here.](https://handbook.gitlab.com/handbook/sales/field-operations/gtm-resources/#criteria-for-outbound-sales-accepted-opportunity-sao)

- Who gets involved while evaluating a tool at {{company}}?
- Would you expect anyone to challenge your initiative, and if so can I help by connecting with anyonse else on your end?
- If you as a {{title}} wanted to purchase GitLab, what process internally would you have to follow, and how can we help you navigate it?
- What challenges do you expect to face when trying to pitch this change internally? Who has a say in this and what do they care about the most?

### Email Writing Cheat Sheet

The table below shows the Command of the Message Email Writing Checklist.

It aims to outline the structure of the emails we write to prospects. Emails should be written in each person’s language and tone and make sure to include the CoM frameworks as per the outline below. You can find additional resources [here.](https://docs.google.com/document/d/1-DF6bEtS9QF9idqBcK77RiLL04CKiFMuc0LDEM5N6RA/edit)

| Subject Line              |                                                   |
|---------------------------|---------------------------------------------------|
| Personalized              | - Use Prospect’s name, Company name, or colleague’s name. - Relate to their situation: Use recent company initiatives, technologies they use, Projects they have planned etc. |
| Curiosity and Urgency     | - Provide an image of their future situation: Guide the reader to imagine how their situation could change. - Use compelling events to give a clear image of where they are currently. |
| CoM                       | - Proof Points: Quote Case Studies or other customers. - How We Do it Better: Make use of our defensible differentiators. |
| Opening Line              |                                                   |
| Avoid cliches             | - Stand out: Avoid losing email real-estate with cliche phrases like “just following up” or “hope all is well”. - Brand yourself: Demonstrate that you have an understanding of the reader’s current situation and a clear idea of his required solutions. |
| CoM                       | - Before Scenarios: Paint a clear image of their current situation and how that is causing them professional or personal pain. - After Scenarios/PBOs: Tie the current situation with a clear image of a future situation that’s beneficial for the business and the individual. |
| Main Body                 |                                                   |
| Addressing Questions and Information provided | - No Free Consulting: Answer questions just enough to give a narrative to your email and tie into the CTA. - No Transactional Answers: Don’t make the reader feel like he’s interacting with an online form, a robot, or a sleazy salesman that doesn’t care. |
| CoM                       | - Discovery Questions: determine which discovery questions tie into their current situation, questions asked or information provided. - Trap-Setting Questions: if competitor technology or objections come up, use trap-setting questions to guide the reader into understanding our differentiators. |
| CTA                       |                                                   |
| Clear Next Step, Agenda and benefit | - Valuable: phrase your CTA in a way that’s clearly valuable to the reader and his specific situation. - Defined: outline what will happen in the next step and how long will it take |
| CoM                       | - Measurable: present metrics or PBOs that will be covered in the next step |

Additionally, you can use the matrix below as a quick-cheet to determine what datapoints can be used for personalization.

| Location               | Information                                                                                                         |
|------------------------|---------------------------------------------------------------------------------------------------------------------|
| LinkedIn               | - Have they recently been promoted? - What are the specific responsibilities stated on their page you are trying to help them with? |
| Company strategy from website | - What are the company’s strategic initiatives? (become more agile?, improve internal business systems?, focus on speed to market on delivery? Streamlining expenditure?) |
| Google search name of contact | - Do they have any blogs? - Have they featured in any articles? - Have they any personal achievements celebrated? - Do they have any side projects? |
| Keyword search company | - “COMPANY” AND (“Agile” OR “digital transformation” OR “DevOps” OR “business process improvement” OR “Cloud” OR “digital strategy”) |
| Leadership Focus       | - Find articles of leadership through boolean searches and ask propose how we can help them achieve these goals |
| Company News           | - Celebrate company achievements                                                                                   |
| Check support tickets  | - Are they experiencing issues that upgrading deployment options could help resolve?                               |
| Mutual Contacts        | - Do you have mutual contacts on LinkedIn you can talk about?                                                      |
| Use cases on website   | - Do they have the same customers as us? How do they help them?                                                    |
| Speak to a user of the tool to create personalized message to decision maker | - Speak to someone who uses out tools every day to uncover pain-points (performance degradation at peak times) and take that information to a decision maker |

## New Outreach Sequence/Workflow creation.

In an effort to better iterate, and refresh, our outreach content and workflows, we have a structured process for BDRs to create/measure new Outreach sequences.

Please log an issue [here](https://gitlab.com/gitlab-com/marketing/sales-development/-/issues/new) by selecting the template `BDR_Sequence_Creation_Request.`

The issue description will walk you through all the next steps.

This process aims to:

**Implement Measurement Clarity and Decision Velocity**

By dogfooding GitLab we have a structured and simplified cadence of quantifying sequence creation in terms of value and results without limiting agency.

**Maintain Equal Contributions in a Shared Reality**
By maintaing a Single Source of Truth (SSoT) we give the best possible forum for cross-functional collaboration with specific guidelines

## Making Changes to the handbook

[Video Walkthrough of how to make changes to the GitLab Handbook for the Sales Development org](https://www.youtube.com/watch?v=P7Nv7bzksiY&t=1032s)

One of our Values is being handbook first. 

In order to align the SDR organization more closely to this ideal, below are suggested steps. Please remember that the Handbook is a living document, and you are strongly encouraged to make improvements and add changes. This is ESPECIALLY true when it comes to net new solutions that should be shared so the whole organization has access to that process. (aka The DevOps ideal of turning “Localized Discoveries” into “Global Knowledge”.)

Steps:

- Have a change you want to make to the handbook? Great!
- Navigate to the source code of the page in the handbook (e.g. Link to edit the SDR page in the Handbook )
- Click either “Edit” or “Web IDE” to make your changes.
- Have a brief but descriptive “Commit message” (e.g. “Add new section on ‘How to Make Changes to the Handbook’”) and commit your changes
- Fill out the Merge Request details

## Follow Up Outreach Sequences

- For SDRS and BDRS that want to sequence leads in their qualifying views we created [this video](https://www.youtube.com/watch?v=IKE24LRJbcc) to help you create a workflow around this follow up [sequence.](https://app1a.outreach.io/sequences/9383)

- The same video also showcases the [Auto Follow Up](https://app1a.outreach.io/sequences/9543) to Book a Meeting Sequence SDRs and BDRs can use when a prospect responds to one of their sequences.

## Automatic Enrollment to Sequences

From FY24Q4 onwards, we will pilot [automatic triggers](https://support.outreach.io/hc/en-us/articles/221361788-How-To-Create-an-Outreach-Trigger) through Outreach. As a first iteration, we will be automatically enrolling leads that indicate a high Propensity to Purchase (PTP) score to a full High Touch outreach sequence. The filters applicable for this trigger are:

- Lead has direct phone in their record
- Lead has a high PTP score (4 or 5)
- Lead is actively enrolled to a Low Touch Sequence

## BDR Territory Movement Rules of Engagement

When a territory moves from one BDR to another but the previous BDR remains with the same team (ex: moves from one MM territory to another) the BDR vacating the territory will have the chance to temporarily retain ownership of accounts with which they have generated ongoing engagement (subject to manager approval). At the time of the holdover, an issue will be created that will be reviewed in 30 days to verify account ownership. This includes:

- The outgoing BDR will be allowed to hold back for 30 days any account with which they can demonstrate two-way engagement that could potentially lead to an IQM and new opportunity. If, after 30 days, no IQM has been set and no stage 0 opportunity exists, the account will be moved by the manager to the new BDR assigned to the territory. If the outgoing BDR is successful in generating a new opportunity, the account will remain with them until the opportunity is either qualified or unqualified.
- The outgoing BDR will be allowed to hold back for 30 days any account with which there is an existing stage 0 opportunity where they are listed as the BDR. After 30 days if the opportunity is not qualified, the account will be moved by the manager to the new BDR assigned to the territory.
- In instances where an account has been reassigned to the new BDR and the outgoing BDR had two-way engagement more than 30 days ago with the account but was unable to convert them to an SAO, and the account later returns unprompted and with no prior activity from the new BDR assigned to the territory and wants to pursue a conversation with sales, the team (manager) will receive SAO credit from any resulting opportunity.
- Any other situations that arise which result in a credit dispute will be investigated and resolved by the BDRs’ leader and senior leader.

## Working with Resellers

The end-user account determines the BDR/SDR alignment: If you are the SDR assigned to the 3rd party lead, please gather the information in point 1 and pass the lead to the correctly assigned BDR (the BDR assigned to the end-user account) who will complete points 2-6

| Step                                           | Description                                                                                                                                                            |
|------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Gather billing and end-user details from the reseller | - Billing company name/address: <br> - Billing company contact/email address: <br> - End-user company name/address: <br> - End-user contact/email address: <br> - Snippet in outreach |
| Create a new lead record with end-user details | - Ensure that all notes are copied over to the new LEAD as this is the LEAD that will be converted.                                                                     |
| Converting the new lead                        | - Name opp to reflect reseller involvement as shown here: “End-user account name via reseller account name” <br> - Convert original reseller lead to a contact associated with the reseller account <br> - If an account does not already exist for the reseller, create one when converting the lead to a contact <br> - Assign the record to the same account owner <br> - Do NOT create a new opportunity with this lead. |
| Attach activity to the opportunity            | - On the reseller contact, go to the activity and link each activity related to your opportunity to the opp. <br> - Activity History > click edit to the left of the activity > choose ‘opportunity’ from the ‘related to’ dropdown > find the new opportunity > save |
| Update the opportunity                         | - Change the business type to new business and stage to pending acceptance. <br> - Under contacts, add the reseller contact, role as reseller, and primary contact. <br> - Under partners, add the reseller account as VAR/Reseller |

## SDR/BDR to AE SAO Handoff Process

The purpose of the BDR-AE outbound handoff process is three fold.

- Ensure Outbound prospects get the best experience possible.
- Ensure the BDR team enjoys operational efficiencies and have clearly structured ways to collaborate with the AE team on outbound accounts.
- Ensure the AE team is set up for success is receiving outbound leads in a structured manner and have the appropriate information to utilize in converting them.

To make sure that the hand-offs maximize internal efficiencies, it is required that:

- The BDR team makes sure to book calls, with a minimum notice time of 48 business hours.
- The BDR team makes sure to properly fulfil Outbound SAO criteria, or work with AE to create a mutual command plan.
- The AE team makes sure to:
    - Accept SAOs within 8 working hours after an IQM call.
    - To leave a chatter note on the opportunity record, tagging BDR and AE manager with feedback on the level of qualification and handoff in case of discrepancies.
    - To be responsible for managing the prospect relationship after all handoff types have taken place. This includes re-scheduling conflicts.

### 1. BDR Qualified Meeting

- Are leads that have been qualified by the BDR over a Discovery call.
- CoM principles have been applied to the call and some of the Before/After Scenarios, Positive Business Outcomes, Requirements and Metrics have been identified and agreed upon between the prospect and the BDR.
- There is a clear need or intent identified from the side of the company, and the prospect has clearly articulated a path to the ultimate decision-maker, as per the guidelines for outbound SAO criteria.


**BDR steps after discovery call**

- Summarize CoM principles uncovered during the call
- Schedule next step through Outreach while being on the call with the prospect
    - Meeting Type should be 45’ Evaluation Orchestration Call and body of invitation should be adjusted to meet the prospect’s needs.
- Send AE Intro Email
    - For demanding hand-offs, customer-facing agenda may also be copied and attached to intro email.
- Log Required SFDC fields and populate Notes field.
- Unless there’s a scheduling conflict for the BDR, attend Evaluation Orchestration Call and kick-off the call:
    - Summarize the BDR qualifying conversation by mentioning and verifying the before and after scenario of the prospect, as transcribed on the SAO notes.
    - After prospect acknowledges that their internal situation have not changes inbetween BDR and AE call, recap the expectations and metrics as transcribed on the SAO notes and handoff to AE to move forward.

#### 2. Joint IQM

- Are meetings booked with leads that are from pre-agreed upon Actively Working Acounts.
- Calls scheduled for these prospects will be taken as a joint IQM with both BDR and AE attending, and leading the qualification.
- CoM principles will be applied during the call, with the goal of uncovering a clear need and path to the ultimate decision-maker, as per the guidelines for outbound SAO criteria.

**BDR steps before discovery call**

- Schedule next step through Outreach while being on the call with the prospect, and schedule straight to the AEs calendar.
    - Meeting Type should be 15’ Discovery Call.
- Create SFDC opportunity, and log any pre-determined or relevant information from your past reseearch.
- Communicate with AE, and create a mutual command plan for the call.
    - When kicking off the call, summarize the BDR research and reason for reaching out, such compelling events, Persons with LIM, etc
    - After prospect acknowledges their compelling event and internal situation, discovery call continues with pre agreed upon structure with AE.

## Organizing a Group Demo 

Organizing a shared demo makes it more simple for us to offer spontaneously a demonstration of GitLab, which should result in a easier handover of opps, and ease to create opps. As BDRs, we arranged the execution of this demo by creating a zoom webinar, and configuring the whole event so that the administrative side is automated.

| |                                                                                                                                                                                                                                                    |
|----------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Define the criteria that qualifies an opportunity as eligible to go to a shared demo.**                                                                                                                                                            |                                                                                                                                                                                                                                                    |
| SAL/AE is involved only                                                    | The SAL can invite his/her/their customers or contacts in order to add value to their ongoing discussion, if the AE/SAL finds it relevant. This is in the context of a conversation with the prospect/customer.                                                                                              |
| SAL/AE and BDR are involved                                                | The BDR schedules a discovery meeting with the SAL (whether it is a prospect or customer) that generates a qualified opportunity. Once the opportunity is created, the SAL/BDR suggests the shared demo instead of the individual demo based on the priority of the opportunity (importance of the account, number of licenses, amount of the opportunity, persona). This is in the context of a conversation with the prospect/customer. There needs to be an opportunity in this case to suggest the demo, for the sake of collaboration between the SAL and the BDR. |
| BDR is involved only                                                       | When a lead with decision power or significant influence in the org (no individual contributor) expresses a high interest for GitLab (aka: high read/open rates on an email, response received by phone with interest confirmed) AND when the BDR has a tangible reason to think that there is a need in the division/team/company (research, information obtained through other leads in the same org), the BDR can leverage the shared demo for HOT leads to create interest or a sense of urgency, the goal being to have the lead jump on an introductory meeting after the shared demo. |
| **Define the criteria for different groups of attendees who can join this demo**                                                                                                                                                                    |                                                                                                                                                                                                                                                    |
| Attendees coming from a BDR conversation                                    | In order to attend a demo, this attendee needs to be part of an opportunity in at least stage 1- Discovery.                                                                                                                                                                                                     |
| Attendees coming from the AE’s conversations                               | The AE can leverage this shared demo to invite his own contacts.                                                                                                                                                                                                                                                  |
| **Step-by-step guide to plan a demo**                                                                                                                                                                                                                  |                                                                                                                                                                                                                                                    |
| 1. Get a Zoom Webinars license                                              | You don’t have a Webinar licence by default, so you need to open an access request issue on GitLab to ask for one.                                                                                                                                                                                            |
| 2. Go on Zoom SSO > Webinars (on the left under Meetings) > Schedule a webinar | You will be able to save the entire configuration of the webinar as a template, in order not to set it up again.                                                                                                                                                                                               |
| 3. Change the topic, description, date/time, and duration                   | Change the topic (this will be the title of your demo, and the name of the event on Google Calendar), the description (describes the content of the demo, the idea is to adapt the focus of the demo depending on your attendees), set the date/time of the demo, and the duration (note that setting a certain duration will not stop the zoom webinar once the time has gone). |
| 4. Tick the box ‘Required’ next to registration                             |                                                                                                                                                                                                                                                    |
| 5. Change the Dial country to match attendees' language                      |                                                                                                                                                                                                                                                    |
| 6. Configure additional settings such as Q&A, recording, and email settings |                                                                                                                                                                                                                                                    |
| 7. Save the configuration as a template for future use                       |                                                                                                                                                                                                                                                    |
## White Glove Event Follow Up Sequence (SAEs Included)

For more personal events, such as Executive Roundtables, there many be more detailed Last Event Notes that have an ask to include the people who interacted with the prospect at the event. For these situations we have a [specialized white glove sequence.](https://app1a.outreach.io/sequences/12883)

 The goal of this sequence is to reach out to the prospect quickly and to include our SAEs/AEs in the conversation.

- The first step is to put the prospect in the Sequence. From there customize the first email step to include the people mentioned in the Last Event Notes. You can also delay the time that the first email goes out so you have more time to get feedback from the SAE/AE if needed.
- Next send a screenshot of the first email step to the SAE/AE’s that will be CC’d. Explain to them that this strategy creates space for them to engage with more specific messaging if the prospect is unresponsive. NOTE: There is a generic task on Day 12 to reach out to the SAE/SA if there has not been any engagement. The goal is to keep them in the loop and for you to work together to engage the prospect.

## Outbound Account Ranking Matrix

This complementary table can help structure your thinking around what outbound accounts to choose.

| Account Scoring & Prioritization |
|----------------------------------|
| **Priority 1 Actively Working Accounts (15%)** |
| - Strong ICP Qualities |
| - Compelling Triggers |
| - Strategy is Tailored and Targeted |
| - Future based next step dates |
| - Specific next step notes |
| **Priority 2 Actively Working Accounts (35%)** |
| - ICP qualities |
| - No Triggers |
| - Strategy is Targeted (persona/industry based) |
| - Future based next step dates updated weekly/bi-weekly |
| - Next step notes should just include week # |
| **Priority 3 Actively Working Accounts (50%)** |
| - ICP qualities |
| - No Recent Triggers |
| - Strategy is nurture based |
| - Future based next step dates updated monthly |
| - Next step notes should just include recent release # or event invite |
| **Other Good Account Scoring Qualifiers:** |
| - Current CE Usage |
| - 250+ employees in IT/TEDD positions |
| - Good Fit Industry / Vertical (High Growth, Technology, Financial, Healthcare, Regulated Business) |
| - Early Adopters / Innovative IT Shops (Identifiers & Keywords): Kubernetes / Containers, Microservices, Multi-cloud, DevOps, DevSecOps, CICD (and open-source + proprietary tools), SAST / DAST, Digital Transformation |
| - Current DevOps Adoption (multiple DevOps roles on staff or hiring for multiple DevOps positions) |
| **Outbound Prospecting Framework** |
| **Priority Level** | **Account Strategy Includes** | **Next Step Date** | **Next Step Notes** |
| Priority 1 | Priority #, Any Details | Future based next steps | Hyper-personalized, simultaneous outreach, creative, direct mail, ads, groundswell, events |
| Priority 2 | Priority #, Weekly Plan or Length of Sequence | Future based next steps depending on account strategy | Include the weeks outreach and overall strategy |
| Priority 3 | Priority # | Update Next Step Date to next months outreach | BDR Next Step, What kind of nurture (Product Updates, Events, Ect) |

## Growth Strategies, Ranking and RoE 


**Strategies:** 

- Growth within an existing customer account: BDR should strategically partner with SAE/ISR to identify additional business units to bring into the conversation as growth opportunities. Renewal dates should be exploited.
- Expansion into parent/children/subsidiaries of existing customer accounts: These are accounts that fall within the corporate hierarchy within Salesforce. If a child/subsidiary account is identified via ZoomInfo but not in our system, then BDR should follow the following process: Create a LEAD for that account and then convert to CONTACT to create the ACCOUNT when you qualify your first lead.
- Free to paid upgrades: Existing Core/CE users can be targeted to upgrade to a paid version of GitLab

**Ranking and prioritization of accounts:**

- Rank 1: Expand with Purpose (EwP) accounts should be closely/strategically worked with SAEs. These accounts have High 6Sense intent scores and should therefore be High touch by BDR.
- Rank 2: ICP Ultimate parent accounts that have Core/CE-Users in a hierarchy, Total CARR/LAM for account, Med/Low 6Sense intent scores, and small renewals in current FQ.
- Rank 3: As above but are not our ICP

**Professional Services Opportunities**

A Professional Services Opportunity will be used to cover any integration, consulting, training or other service that a Sales rep will sell to a prospect/client and needs or wants to be invoiced separately. To invoice separately a new quote and opportunity must be created.


## Frequently Used Terms

| Term                      | Definition                                                                                                                                                                  |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Accepted Lead             | A lead that an SDR or BDR agrees to work until qualified in or qualified out                                                                                               |
| Account                   | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller                         |
| AM                        | Account Manager                                                                                                                                                             |
| AE                        | Account Executive, can be Major or Strategic in AMER/EMEA Enterprise                                                                                                       |
| APAC                      | Asia-Pacific                                                                                                                                                                |
| BDR                       | Business Development Represenative - focused on outbound                                                                                                                     |
| CAM                       | Channel Account Manager                                                                                                                                                    |
| CS                        | Customer Success                                                                                                                                                           |
| DACH                      | Germany, Austria and Switzerland region                                                                                                                                     |
| EMEA                      | Europe, Middle East, and Africa                                                                                                                                            |
| EULA                      | End User License Agreement                                                                                                                                                  |
| High intent               | An event, webcast, demo that is a strong indicator of purchase or evaluation                                                                                                |
| Inquiry                   | An Inbound request or response to an outbound marketing effort                                                                                                              |
| IQM                       | Initial Qualifying Meeting                                                                                                                                                  |
| LATAM                     | Latin America (includes all of Central & South America)                                                                                                                     |
| META                      | Middle East, Turkey and Africa Region                                                                                                                                       |
| MQL                       | Marketing Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behavior lead scoring)                        |
| MVC                       | Minimal Viable Change (not Model View Controller)                                                                                                                           |
| NEUR                      | Northern Europe Region                                                                                                                                                      |
| NORAM                     | North America                                                                                                                                                               |
| Qualified Lead            | A lead that a BDR or SDR has qualified, converted to an opportunity and assigned to a Sales Representative (Stage 0-Pending Acceptance)                                     |
| RD                        | Regional Director                                                                                                                                                           |
| ROW                       | Rest of World                                                                                                                                                               |
| SAE                       | Strategic Account Executive                                                                                                                                                |
| Sales Admin               | Sales Administrator                                                                                                                                                         |
| Sales Serve               | A sales method where a quota bearing member of the sales team closes the deal                                                                                                |
| SAO                       | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting                                                                  |
| SDR                       | Sales Development Representative - focused on inbound                                                                                                                        |
| Self Serve                | A sales method where a customer purchases online through our web store. Also known as a “web direct”                                                                        |
| SEUR                      | Southern Europe Region                                                                                                                                                      |
| SLA                       | Service Level Agreement                                                                                                                                                     |
| SQO                       | Sales Qualified Opportunity                                                                                                                                                 |
| CSM                       | Customer Success Manager                                                                                                                                                   |
| TEDD                      | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company                                                       |
| Won Opportunity           | Contract signed to Purchase GitLab                                                                                                                                          |
| Hyper-personalization    | The concept of combining real-time data extracted from multiple sources to create outreach that resonates with prospects on an individual level.                             |
| VIP (prospect)           | A Very Important Person, top officer, executive buyer, C-level prospect, or important influencer requiring hyper-personalization.                                            |
| Influencer (prospect)    | An individual prospect suspected to be involved with IT decision-making, tooling, teams, roadmap, strategic projects, and/or budgets.                                         |
| User (prospect)          | A prospect with limited influence within an IT organization.                                                                                                                 |
| Groundswell               | An outbound strategy focused on filling the top of the funnel by generating engagement, opt-ins, MQLs, and uncovering intent signals.                                         |
| Snippets                  | Content created for BDRs and SDRs to use to create hyper-personalized sequences, one-off emails, or to use for reaching out to prospects via LinkedIn.                      |
| Warm Calling              | The method used to strategically incorporate phone calls and voicemails into an outbound prospecting workflow.                                                               |
| Rules of Engagement (RoE) | Defines ownership of a lead, SAO credit, and collaboration between SDRs and BDRs.                                                                                          |
| BDR Prospecting Status    | Salesforce status includes Queued, Actively Working, Worked in FY, and Restricted.                                                                                         |
| Partner Prospecting Status| If populated, the lead/contact is worked by a partner, and Sales Dev should not proceed with outreach.                                                                     |
| Actively Working Status   | Accounts chosen for strategic outreach based on alignment, news, initiatives, etc. Recycles after 10 weeks if not worked.                                                    |
| Actively Working Status Requirements | BDR Account Strategy field must be populated. Research and Next Steps notes within 10 days. Minimum 5 people in sequences.                                               |
| Queued status             | Accounts waiting to be moved into Actively Working. SDRs work MQLs in this status.                                                                                          |
| Worked in FY status       | Indicates an account went through “Actively Working” this FY. Can be moved back to “Actively Working” later.                                                               |
| Restricted status          | SAE-indicated restriction on the account. BDRs handle the status change, note the reason, and reroute to assigned BDR.                                                      |





